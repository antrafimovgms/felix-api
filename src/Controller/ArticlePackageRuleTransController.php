<?php

namespace App\Controller;


use App\Repository\ArticlePackageRuleTransRepository;
use App\Repository\BaseRepository;

class ArticlePackageRuleTransController extends SaverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new ArticlePackageRuleTransRepository($this->getDoctrine()->getConnection());
    }
}