<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\PreisberechnungRepository;

class PreisberechnungController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new PreisberechnungRepository($this->getDoctrine()->getConnection());
    }
}