<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\SpracheArrangementRepository;

class SpracheArrangementController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new SpracheArrangementRepository($this->getDoctrine()->getConnection());
    }
}