<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\MeldescheinDatenRepository;

class MeldescheinDatenController extends ReceiverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new MeldescheinDatenRepository($this->getDoctrine()->getConnection());
    }

    /**
     * (@inheritDoc)
     */
    public function getSearchCriteria(): array
    {
        return [
            'exportiert' => 0
        ];
    }
}