<?php


namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\JufaSeasonsRepository;

class JufaSeasonsController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new JufaSeasonsRepository($this->getDoctrine()->getConnection());
    }
}