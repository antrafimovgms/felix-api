<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\BookingsRepository;

class BookingsController extends ReceiverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new BookingsRepository($this->getDoctrine()->getConnection());
    }

    /**
     * (@inheritDoc)
     */
    public function getSearchCriteria(): array
    {
        return [
            'exportiert' => 0
        ];
    }
}