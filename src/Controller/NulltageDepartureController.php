<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\NulltageDepartureRepository;

class NulltageDepartureController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new NulltageDepartureRepository($this->getDoctrine()->getConnection());
    }
}