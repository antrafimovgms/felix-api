<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\BookingsRoomsRepository;

class BookingsRoomsController extends ReceiverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new BookingsRoomsRepository($this->getDoctrine()->getConnection());
    }

    /**
     * (@inheritDoc)
     */
    public function getSearchCriteria(): array
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        return [
            'booking_id' => $request->get('booking_id')
        ];
    }
}