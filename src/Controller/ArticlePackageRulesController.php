<?php

namespace App\Controller;


use App\Repository\ArticlePackageRulesRepository;
use App\Repository\BaseRepository;

class ArticlePackageRulesController extends SaverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new ArticlePackageRulesRepository($this->getDoctrine()->getConnection());
    }
}