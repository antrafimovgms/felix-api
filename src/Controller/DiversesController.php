<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\DiversesRepository;

class DiversesController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new DiversesRepository($this->getDoctrine()->getConnection());
    }
}