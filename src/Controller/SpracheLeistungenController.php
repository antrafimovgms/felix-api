<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\SpracheLeistungenRepository;

class SpracheLeistungenController extends SaverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new SpracheLeistungenRepository($this->getDoctrine()->getConnection());
    }
}