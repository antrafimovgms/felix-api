<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\RoomplanRepository;

class RoomplanController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new RoomplanRepository($this->getDoctrine()->getConnection());
    }
}