<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\BuchtageRepository;

class BuchtageController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new BuchtageRepository($this->getDoctrine()->getConnection());
    }
}