<?php

namespace App\Controller;


use App\Service\FirmIdentityAdapter;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class SaverController extends BaseController
{
    /**
     * Remove records from the table
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ErrorException
     */
    public function clear(Request $request)
    {
        $hotel = $this->getHotel($request);

        try {
            $repo = $this->getRepository();
            $repo->removeByHotel($hotel['gob_hotel_id']);
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return new JsonResponse([
            'message' => 'ok'
        ]);
    }

    /**
     * Load batch to the table
     * @param Request $request
     * @param LoggerInterface $logger
     * @param FirmIdentityAdapter $adapter
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ErrorException
     */
    public function add(Request $request, LoggerInterface $logger, FirmIdentityAdapter $adapter)
    {
        $repo = $this->getRepository();
        $data = json_decode($request->getContent(), true);

        $logger->info('New batch received to '.$repo::$table, [
            'headers' => json_encode($request->headers->all()),
            'data' => $request->getContent(),
            'server' => json_encode($_SERVER),
        ]);

        if (!is_array($data) || empty($data)) {
            throw new BadRequestHttpException('data is required and should be an array');
        }

        try {
            $repo->insertBatch($adapter->adapt($data, FirmIdentityAdapter::CONTEXT_API, [
                'hotelIdField' => $repo->getHotelIdField()
            ]));
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return new JsonResponse([
            'message' => 'ok'
        ]);
    }
}