<?php


namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\RoomCategoryPricesRepository;

class RoomCategoryPricesController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new RoomCategoryPricesRepository($this->getDoctrine()->getConnection());
    }
}