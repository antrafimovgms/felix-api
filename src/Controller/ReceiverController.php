<?php

namespace App\Controller;


use App\Service\FirmIdentityAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class ReceiverController extends BaseController
{
    /**
     * @param Request $request
     * @param FirmIdentityAdapter $adapter
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ErrorException
     */
    public function receive(Request $request, FirmIdentityAdapter $adapter)
    {
        $hotel = $this->getHotel($request);
        $repo = $this->getRepository();

        try {
            $searchCriteria = array_merge($this->getSearchCriteria(), [
                $repo->getHotelIdField() => $hotel['gob_hotel_id'],
            ]);

            $batch = $adapter->adapt((array)$repo->getBatch($searchCriteria), FirmIdentityAdapter::CONTEXT_FELIX, [
                'hotelIdField' => $repo->getHotelIdField()
            ]);

        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return new JsonResponse($batch);
    }

    /**
     * Mark records as read
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ErrorException
     */
    public function setReadFlag(Request $request)
    {
        $hotel = $this->getHotel($request);
        $data = json_decode($request->getContent(), true);

        if (!is_array($data) || empty($data)) {
            throw new BadRequestHttpException('data is required and should be an array');
        }

        try {
            $repo = $this->getRepository();
            $repo->setReadFlag($data, $hotel['gob_hotel_id']);
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return new JsonResponse([
            'message' => 'ok'
        ]);
    }

    /**
     * retrieve search criteria from request
     * @return array
     */
    abstract protected function getSearchCriteria() : array;
}