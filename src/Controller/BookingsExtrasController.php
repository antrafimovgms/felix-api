<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\BookingsExtrasRepository;

class BookingsExtrasController extends ReceiverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new BookingsExtrasRepository($this->getDoctrine()->getConnection());
    }

    /**
     * (@inheritDoc)
     */
    public function getSearchCriteria(): array
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        return array_filter([
            'booking_id' => $request->get('booking_id'),
            'booking_room_id' => $request->get('booking_room_id')
        ]);
    }
}