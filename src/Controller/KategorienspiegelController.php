<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\KategorienspiegelRepository;

class KategorienspiegelController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new KategorienspiegelRepository($this->getDoctrine()->getConnection());
    }
}