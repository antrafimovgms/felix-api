<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\SpracheKategorieRepository;

class SpracheKategorieController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new SpracheKategorieRepository($this->getDoctrine()->getConnection());
    }
}