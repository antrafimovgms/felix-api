<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\RoomsRepository;

class RoomsController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new RoomsRepository($this->getDoctrine()->getConnection());
    }
}