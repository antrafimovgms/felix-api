<?php


namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\PreiseaktionenRepository;

class PreiseaktionenController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new PreiseaktionenRepository($this->getDoctrine()->getConnection());
    }
}