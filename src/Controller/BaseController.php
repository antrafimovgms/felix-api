<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\HotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BaseController
 * @package App\Controller
 */
abstract class BaseController extends AbstractController implements TokenAuthenticatedController
{
    /**
     * Get auth token
     * @param Request $request
     * @return false|string|null
     */
    public function getToken(Request $request)
    {
        $token = $request->headers->get('Authorization');
        return strlen($token) ? substr($token, 7) : null;
    }

    /**
     * @param Request $request
     * @return false|mixed[]
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ErrorException
     */
    public function getHotel(Request $request)
    {
        return (new HotelRepository($this->getDoctrine()->getConnection()))
            ->findHotelByToken($this->getToken($request));
    }

    /**
     * Init related storage
     * @return BaseRepository
     * @throws \ErrorException
     */
    abstract protected function getRepository() : BaseRepository;
}