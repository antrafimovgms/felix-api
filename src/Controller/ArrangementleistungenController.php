<?php

namespace App\Controller;


use App\Repository\ArrangementleistungenRepository;
use App\Repository\BaseRepository;

class ArrangementleistungenController extends SaverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new ArrangementleistungenRepository($this->getDoctrine()->getConnection());
    }
}