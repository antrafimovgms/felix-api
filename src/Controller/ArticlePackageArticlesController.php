<?php


namespace App\Controller;


use App\Repository\ArticlePackageArticlesRepository;
use App\Repository\BaseRepository;

class ArticlePackageArticlesController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new ArticlePackageArticlesRepository($this->getDoctrine()->getConnection());
    }
}