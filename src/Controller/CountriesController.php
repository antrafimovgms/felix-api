<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\CountriesRepository;

class CountriesController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new CountriesRepository($this->getDoctrine()->getConnection());
    }
}