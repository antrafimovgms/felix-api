<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\KategoriepreisRepository;

class KategoriepreisController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new KategoriepreisRepository($this->getDoctrine()->getConnection());
    }
}