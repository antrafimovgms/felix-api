<?php


namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\LookupCompaniesRepository;

class LookupCompaniesController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new LookupCompaniesRepository($this->getDoctrine()->getConnection());
    }
}