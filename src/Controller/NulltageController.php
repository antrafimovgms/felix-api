<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\NulltageRepository;

class NulltageController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new NulltageRepository($this->getDoctrine()->getConnection());
    }
}