<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\LastminuteRepository;

class LastminuteController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new LastminuteRepository($this->getDoctrine()->getConnection());
    }
}