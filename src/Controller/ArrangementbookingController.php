<?php

namespace App\Controller;


use App\Repository\ArrangementbookingRepository;
use App\Repository\BaseRepository;

class ArrangementbookingController extends ReceiverController
{
    /**
     * (@inheritDoc)
     */
    public function getRepository(): BaseRepository
    {
        return new ArrangementbookingRepository($this->getDoctrine()->getConnection());
    }

    /**
     * (@inheritDoc)
     */
    public function getSearchCriteria(): array
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        return [
            'booking_room_id' => $request->get('booking_room_id')
        ];
    }
}