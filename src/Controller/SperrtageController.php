<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\SperrtageRepository;

class SperrtageController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new SperrtageRepository($this->getDoctrine()->getConnection());
    }
}