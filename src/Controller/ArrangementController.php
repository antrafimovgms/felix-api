<?php

namespace App\Controller;


use App\Repository\ArrangementRepository;
use App\Repository\BaseRepository;

class ArrangementController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new ArrangementRepository($this->getDoctrine()->getConnection());
    }
}