<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\ZimmertageRepository;

class ZimmertageController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new ZimmertageRepository($this->getDoctrine()->getConnection());
    }
}