<?php

namespace App\Controller;


use App\Repository\BaseRepository;
use App\Repository\ExtrasRepository;

class ExtrasController extends SaverController
{
    public function getRepository(): BaseRepository
    {
        return new ExtrasRepository($this->getDoctrine()->getConnection());
    }
}