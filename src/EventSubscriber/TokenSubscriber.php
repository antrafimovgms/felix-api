<?php

namespace App\EventSubscriber;


use App\Controller\TokenAuthenticatedController;
use App\Repository\HotelRepository;
use Doctrine\DBAL\Connection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class TokenSubscriber implements EventSubscriberInterface
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();

        // when a controller class defines multiple action methods, the controller
        // is returned as [$controllerInstance, 'methodName']
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        if ($controller instanceof TokenAuthenticatedController) {

            $token = $event->getRequest()->headers->get('Authorization');
            $token = strlen($token) ? substr($token, 7) : null;
            $hotelRepo = new HotelRepository($this->connection);

            if (!strlen($token) || !$hotelRepo->findHotelByToken($token)) {
                throw new AccessDeniedHttpException('You need permission to perform this call.');
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}