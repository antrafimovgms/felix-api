<?php

namespace App\Service;


use App\Repository\InteractionMapRepository;

class FirmIdentityAdapter implements DataAdapterInterface
{
    const CONTEXT_FELIX = 'felix';
    const CONTEXT_API = 'api';

    private InteractionMapRepository $interactionMapRepo;
    private HotelLookupper $hotelLookupper;

    /**
     * FirmIdentityAdapter constructor.
     * @param InteractionMapRepository $interactionMapRepo
     * @param HotelLookupper $hotelLookupper
     */
    public function __construct(InteractionMapRepository $interactionMapRepo, HotelLookupper $hotelLookupper)
    {
        $this->interactionMapRepo = $interactionMapRepo;
        $this->hotelLookupper = $hotelLookupper;
    }

    /**
     * (@inheritDoc)
     */
    public function adapt(array $data, string $context, array $params = []): array
    {
        $hotel = $this->hotelLookupper->current();
        $hotelId = $hotel['gob_hotel_id'];;

        if ($context === static::CONTEXT_FELIX) {
            $interactionMap = $this->interactionMapRepo->findHotelByMasterId($hotel['gob_hotel_id']);
            $hotelId = $interactionMap['felix_firm_id'];
        }

        $data = array_map(function ($item) use ($hotelId, $params) {

            $item = array_change_key_case($item, CASE_LOWER);
            $hotelIdField = strtolower($params['hotelIdField']);

            if (array_key_exists($hotelIdField, $item)) {
                $item[strtolower($params['hotelIdField'])] = $hotelId;
            }

            return $item;
        }, $data);

        return $data;
    }
}