<?php

namespace App\Service;


use App\Repository\HotelRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class HotelLookupper
{
    public Request $request;
    public HotelRepository $repo;

    /**
     * HotelLookupper constructor.
     * @param RequestStack $requestStack
     * @param HotelRepository $repo
     */
    public function __construct(RequestStack $requestStack, HotelRepository $repo)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->repo = $repo;
    }

    public function current()
    {
        $token = $this->request->headers->get('Authorization');
        $token = strlen($token) ? substr($token, 7) : null;

        return $token
            ? $this->repo->findHotelByToken($token)
            : null;
    }
}