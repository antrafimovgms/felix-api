<?php

namespace App\Service;


interface DataAdapterInterface
{
    public function adapt(array $data, string $context, array $params = []): array;
}