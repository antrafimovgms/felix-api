<?php

declare(strict_types=1);

namespace DoctrineMigrations;


use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200508083219 extends AbstractMigration
{
    private $triggerName = 'hotel_before_insert_generate_auth_token';

    public function getDescription() : string
    {
        return 'Create trigger to generate auth tokens for new record in hotel table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TRIGGER '.$this->triggerName.'
            BEFORE INSERT ON hotel FOR EACH ROW 
            BEGIN
              IF new.auth_token IS NULL THEN
                SET new.auth_token = MD5(UUID());
              END IF;
            END;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TRIGGER IF EXISTS '.$this->triggerName);
    }
}
