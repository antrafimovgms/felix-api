<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200623114240 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Increase size to 255 for column hotelhotel_booking_sprache_leistungen.BEZEICHNUNG';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `hotelhotel_booking_sprache_leistungen` CHANGE `BEZEICHNUNG` `BEZEICHNUNG` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `hotelhotel_booking_sprache_leistungen` CHANGE `BEZEICHNUNG` `BEZEICHNUNG` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
    }
}
