<?php

declare(strict_types=1);

namespace DoctrineMigrations;


use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200508064934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Update table hotel: add new column auth_token for API authorization';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE hotel ADD COLUMN auth_token VARCHAR(255) NULL UNIQUE AFTER encryption_key;');
        $this->addSql('UPDATE IGNORE hotel SET auth_token = MD5(UUID()) WHERE auth_token IS NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE hotel DROP COLUMN IF EXISTS auth_token;');
    }
}
