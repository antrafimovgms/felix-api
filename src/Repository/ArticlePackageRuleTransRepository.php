<?php


namespace App\Repository;


class ArticlePackageRuleTransRepository extends BaseRepository
{
    public static $table = 'hotelhotel_article_package_rule_trans';

    /**
     * Return hotel id field name
     * @return string
     */
    public function getHotelIdField()
    {
        return 'lookup_company_id';
    }

    /**
     * (@inheritDoc)
     */
    public function removeByHotel(int $hotelId)
    {
        $qb = $this->connection->createQueryBuilder();

        return $qb->delete(self::$table)
            ->where(
                $qb->expr()->in(
                    'article_package_rule_id',
                    'SELECT id FROM '.ArticlePackageRulesRepository::$table.' WHERE '.$this->getHotelIdField().' = :'.$this->getHotelIdField()
                )
            )
            ->setParameter($this->getHotelIdField(), $hotelId)
            ->execute();
    }
}