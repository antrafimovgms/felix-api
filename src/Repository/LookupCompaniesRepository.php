<?php

namespace App\Repository;


class LookupCompaniesRepository extends BaseRepository
{
    public static $table = 'hotellookup_companies';

    /**
     * (@inheritDoc)
     */
    public function getHotelIdField()
    {
        return 'id';
    }
}