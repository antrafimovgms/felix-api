<?php


namespace App\Repository;


class JufaSeasonsRepository extends BaseRepository
{
    public static $table = 'JUFA_SEASONS';

    /**
     * Return hotel id field name
     * @return string
     */
    public function getHotelIdField()
    {
        return 'lookup_company_id';
    }

}