<?php

namespace App\Repository;


class RoomplanRepository extends BaseRepository
{
    public static $table = 'hotelhotel_booking_roomplan';

    /**
     * (@inheritDoc)
     */
    public function getHotelIdField()
    {
        return 'lookup_company_id';
    }
}