<?php

namespace App\Repository;


use Doctrine\DBAL\Connection;

abstract class BaseRepository
{
    /**
     * @var string DB table name
     */
    public static $table = '';

    /**
     * @var Connection DB connection
     */
    protected $connection;

    /**
     * @var array Fields list
     */
    private $fields = [];


    /**
     * BaseRepository constructor.
     * @param Connection $connection
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ErrorException
     */
    public function __construct(Connection $connection)
    {
        if (
            !strlen(static::$table) ||
            empty($this->fields = array_keys($connection->getSchemaManager()->listTableColumns(static::$table)))
        ) {
            throw new \ErrorException(static::class.':__construct - Invalid table name ['.static::$table.']');
        }

        $connection->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        $this->connection = $connection;
    }

    /**
     * Get the table fields list
     * @return array
     */
    public function getTableFields()
    {
        return $this->fields;
    }

    /**
     * Return hotel id field name
     * @return string
     */
    public function getHotelIdField()
    {
        return 'firma';
    }

    /**
     * Remove the data by hotel identity
     * @param int $hotelId
     * @return int Affected rows
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeByHotel(int $hotelId)
    {
        return $this->connection->delete(static::$table, array($this->getHotelIdField() => $hotelId));
    }

    /**
     * Insert batch into the table
     * @param array $batch
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function insertBatch(array $batch)
    {
        $this->connection->beginTransaction();

        try{

            foreach ($batch as $row) {
                $qb = $this->connection->createQueryBuilder()->insert(static::$table);
                foreach ($row as $key => $val) {
                    $qb = $qb->setValue($key, ':'.$key)->setParameter($key, $val);
                }
                $qb->execute();
            }

            $this->connection->commit();
            return true;
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw $e;
        }
    }

    /**
     * Receive batch from the table
     * @param array $conditions
     * @return mixed[]
     */
    public function getBatch(array $conditions = [])
    {
        $qb = $this
            ->connection
            ->createQueryBuilder()
            ->select('*')
            ->from(static::$table);

        foreach ($conditions as $field => $val) {
            $qb
                ->andWhere($field.' = :'.$field)
                ->setParameter($field, $val);
        }

        return $qb->execute()->fetchAll();
    }

    /**
     * Mark records as read
     * @param array $data
     * @param int $hotelId
     * @return bool|\Doctrine\DBAL\Driver\ResultStatement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setReadFlag(array $data, int $hotelId)
    {
        if (!empty($data)) {
            $this->connection->executeQuery('UPDATE '.static::$table.' SET exportiert = 1 WHERE id IN (?) AND '.$this->getHotelIdField().' = ?',
                [$data, $hotelId],
                [Connection::PARAM_INT_ARRAY]
            )->columnCount();
            return true;
        }

        return false;
    }
}