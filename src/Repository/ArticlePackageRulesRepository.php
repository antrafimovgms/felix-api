<?php


namespace App\Repository;


class ArticlePackageRulesRepository extends BaseRepository
{
    public static $table = 'hotelhotel_article_package_rules';

    /**
     * Return hotel id field name
     * @return string
     */
    public function getHotelIdField()
    {
        return 'lookup_company_id';
    }
}