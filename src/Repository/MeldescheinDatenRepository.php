<?php

namespace App\Repository;


class MeldescheinDatenRepository extends BaseRepository
{
    public static $table = 'hotel_meldeschein_daten';
    public static $tableAlias = 'hmd';

    /**
     * Receive batch from the table
     * @param array $conditions
     * @return mixed[]
     */
    public function getBatch(array $conditions = [])
    {
        $qb = $this
            ->connection
            ->createQueryBuilder()
            ->select(self::$tableAlias.'.*, hm.gastid')
            ->from(static::$table, self::$tableAlias)
            ->leftJoin(self::$tableAlias, 'hotel_meldeschein', 'hm', self::$tableAlias.'.meldeschein_id=hm.id');

        foreach ($conditions as $field => $val) {
            $qb
                ->andWhere(self::$tableAlias.'.'.$field.' = :'.$field)
                ->setParameter($field, $val);
        }

        return $qb->execute()->fetchAll();
    }
}