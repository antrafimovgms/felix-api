<?php

namespace App\Repository;


class HotelRepository extends BaseRepository
{
    public static $table = "hotel";

    public function findHotelByToken(string $token)
    {
        return $this->connection->fetchAssoc('SELECT * FROM '.self::$table.' WHERE auth_token = ?', [$token]);
    }
}