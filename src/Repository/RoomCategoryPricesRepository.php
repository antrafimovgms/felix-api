<?php


namespace App\Repository;


class RoomCategoryPricesRepository extends BaseRepository
{
    public static $table = 'ROOM_CATEGORY_PRICES';

    /**
     * Return hotel id field name
     * @return string
     */
    public function getHotelIdField()
    {
        return 'lookup_company_id';
    }

}