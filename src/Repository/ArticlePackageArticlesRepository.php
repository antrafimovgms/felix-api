<?php


namespace App\Repository;


class ArticlePackageArticlesRepository extends BaseRepository
{
    public static $table = 'ARTICLE_PACKAGE_ARTICLES';

    /**
     * Return hotel id field name
     * @return string
     */
    public function getHotelIdField()
    {
        return 'lookup_company_id';
    }
}