<?php

namespace App\Repository;


class InteractionMapRepository extends BaseRepository
{
    public static $table = "interaction_map";

    /**
     * @param int $masterId
     * @return false|mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findHotelByMasterId(int $masterId)
    {
        return $this->connection->fetchAssoc('SELECT * FROM '.self::$table.' WHERE master_firm_id = ?', [$masterId]);
    }
}